import json
import pandas as pd
import argparse
import logging
import sys,os
from versa_director_21_2_3 import director_21_2_3

class interface_status_cls():

    def __init__(self, **kwargs):
        parser = self.get_args()
        args = parser.parse_args()        
        self.appliance_all_info_raw_ = []
        self.appliance_all_info_raw_dict = {}        
        self.director_ip = args.director_ip
        self.username = args.username
        self.password = args.password
        self.vd = director_21_2_3(self.director_ip,self.username, self.password)
        if os.path.exists('interface_status_script.log'):
            os.system("rm -rf interface_status_script.log")        
        else:
            pass
        self.log_setup()

    def get_args(self):
        parser = argparse.ArgumentParser(description='Interface Status Script')

        parser.add_argument('-vd', '--director_ip',required=True,
                            help="Director IP")
        parser.add_argument('-vdu', '--username',required=True,
                            help="Director Username")
        parser.add_argument('-pwd', '--password',required=True,
                            help="Director Password")                        
        return parser          

    def log_setup(self):   
        file_handler = logging.FileHandler("interface_status_script.log")
        console_handler = logging.StreamHandler(sys.stdout)
        handelers = [file_handler, console_handler]
        logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s", handlers=handelers)

    def director_package_info(self):
        
        logging.info('Director package check'.title())
        director_package_info = json.dumps(self.vd.director_package_info())
        director_package_info = json.loads(director_package_info)
        if director_package_info == None:
            exit()
        for key in director_package_info:
            if key == 'error':    
                logging.error('{}'.title().format(director_package_info["error"]["description"]))
                exit()
            else:
                director_major_version = director_package_info["package-info"][0]["major-version"]
                director_minor_version = director_package_info["package-info"][0]["minor-version"]
                director_service_version = director_package_info["package-info"][0]["service-version"]
                if int(director_major_version) == 21 and int(director_minor_version) == 2 and int(director_service_version) == 1:
                    logging.info('Director package is in 21.2.1'.title())
                    self.appliance_all_info_21_2_1()
                elif int(director_major_version) == 21 and int(director_minor_version) == 2 and int(director_service_version) == 2:
                    logging.info('Director package is in 21.2.2'.title())
                    self.appliance_all_info_21_2_2()
                else:
                    logging.error('Director package is not in 21.2.1 or 21.2.2')
                    exit()

    def appliance_all_info_21_2_3(self):
        logging.info('Getting all the device details from the director'.title())
        appliance_all_info = json.dumps(self.vd.appliance_all_info())
        appliance_all_info = json.loads(appliance_all_info)
        appliance_all_info = appliance_all_info["versanms.ApplianceStatusResult"]["appliances"]
        for appliance_info in appliance_all_info:
            self.appliance_all_info_raw_dict['name'] = appliance_info["name"]
            # self.appliance_all_info_raw_dict['uuid'] = appliance_info["uuid"]
            self.appliance_all_info_raw_dict['ping-status'] = appliance_info["ping-status"]
            self.appliance_all_info_raw_dict['sync-status'] = appliance_info["sync-status"]
            self.appliance_all_info_raw_dict['services-status'] = appliance_info["services-status"]
            self.appliance_all_info_raw_dict['ownerOrg'] = appliance_info["ownerOrg"]
            self.appliance_all_info_raw_.append(self.appliance_all_info_raw_dict.copy())
            if appliance_info["ping-status"] == "REACHABLE":
                self.device_interface_status_details(appliance_info["name"],appliance_info["ownerOrg"])

    def device_interface_status_details(self,deviceworkflowname,org_name):
        logging.info('Checking device interface status for {}'.title().format(deviceworkflowname))
        device_interface_status = json.dumps(self.vd.device_interface_status(deviceworkflowname,org_name))
        device_interface_status_detail = json.loads(device_interface_status)        
        device_interface_status = device_interface_status_detail['List']['value']
        for device_interface_status_detail in device_interface_status:
            self.appliance_all_info_raw_dict['Interface'] = device_interface_status_detail["name"]
            self.appliance_all_info_raw_dict['Interface_Type'] = device_interface_status_detail["type"]
            self.appliance_all_info_raw_dict['Oper_Status'] = device_interface_status_detail["ifOperStatus"]
            self.appliance_all_info_raw_dict['Admin_Status'] = device_interface_status_detail["ifAdminStatus"]
            self.appliance_all_info_raw_dict['AdminUp'] = device_interface_status_detail["adminUp"]
            self.appliance_all_info_raw_dict['OperUp'] = device_interface_status_detail["operUp"]
            self.appliance_all_info_raw_.append(self.appliance_all_info_raw_dict.copy())

            
    def data_write(self):

        interface_status_write = pd.DataFrame(self.appliance_all_info_raw_)
        interface_status_write.to_csv("interface_detail_status.csv", index=False)     
        print(interface_status_write)



def main():
    automation = interface_status_cls()
    automation.log_setup()
    automation.appliance_all_info_21_2_3()
    automation.data_write()   

if __name__ == "__main__":
    main()
