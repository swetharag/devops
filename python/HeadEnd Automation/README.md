
===================================================================
                        he_automation_v1.0
-------------------------------------------------------------------

Version

	v.1.0
Summary

    This Script is intended to Automate and configure the Versa Head End components. The Head End components contains Master and slave director, with 4-node analytics cluster and 2 controllers. service_vnf (DC & DR) is optional.

    Script can be used in 2 uses cases:
        - without Service_vnf  
        - with Service_vnf

    - without Service_vnf

        Script will check the North_Bound reachability of Slave_Director,Analytics_1, Analytics_2, Analytics_3, Analytics_4, controller_1 and controller2 and south_bound reachability of Analytics_1,Analytics_2,Analytics_3,Analytics_4 from master director, if all the reachability is fine, then it will check the North_Bound reachability of Master_Director,Analytics_1,Analytics_2,Analytics_3,Analytics_4, controller_1 and controller2 and south_bound reachability Analytics_1,Analytics_2,Analytics_3,Analytics_4 from slave director, if all the reachability is fine, then it will configure the Versa Director HA, 4 node analytics cluster & 2 Versa Controller's, Once the script is executed successfully, it will generate he_automation.log file.

    - with Service_vnf

        Script will check the Management Reachability of DC_service_vnf and DR_service_vnf from master director, if reachability is fine, it will configure the DC_service_vnf and DR_service_vnf then integrate with director. once service_vnf integration is done, it will check the North_Bound reachability of Slave_Director, Analytics_1, Analytics_2, Analytics_3, Analytics_4, controller_1 and controller2 and south_bound reachability of Analytics_1,Analytics_2,Analytics_3 and Analytics_4 from master director, if all the reachability is fine, then it check the North_Bound reachability of Master_Director,Analytics_1,Analytics_2,Analytics_3 and Analytics_4, controller_1 and controller2 and south_bound reachability of Analytics_1,Analytics_2,Analytics_3 and Analytics_4 from slave director, if all the reachability is fine, then it will configure the Versa Director HA, 4 node analytics cluster & 2 Versa Controller's, Once the script is executed successfully, it will generate he_automation.log file.

Prerequisites

    # Following Pre-requisites has to be fulfilled to execute the script (without Service_vnf).

        - Update the he_automation.csv file with all required info.
        - Master and Slave Director Startup script (vnms-startup.sh) should be executed before running the script.
        - North_Bound (eth0) of Slave Director, Controller_1, Controller_2, Analytics_1, Analytics_2, Analytics_3 and Analytics_4 should be reachable from Master Director.
        - South_Bound (eth1) of Analytics_1, Analytics_2, Analytics_3, Analytics_4 should be reachable from Master Director via south_bound(eth1).
        - North_Bound (eth0) of Master Director, Controller_1, Controller_2, Analytics_1, Analytics_2, Analytics_3 and Analytics_4 should be reachable from Slave Director.
        - South_Bound (eth1) of Analytics_1, Analytics_2, Analytics_3 and Analytics_4 should be reachable from Slave Director via south_bound(eth1).

    # Following Pre-requisites has to be fulfilled to execute the script (with Service_vnf).

        - Update the he_automation.csv file with all required info.
        - Master and Slave Director Startup script (vnms-startup.sh) should be executed before running the script.
        - North_Bound (eth0) of DC_service_vnf and DR_service_vnf should be reachable from Master and slave director.
        - Inter_conncet (eth2/vni-0/1) of DC_service_vnf and DR_service_vnf should be reachable to each other.
        - North_Bound (eth0) of Slave Director, Controller_1, Controller_2, Analytics_1, Analytics_2, Analytics_3 and Analytics_4 should be reachable from Master Director.
        - South_Bound (eth1) of Analytics_1, Analytics_2, Analytics_3 and Analytics_4 should be reachable from Master Director via south_bound(eth1).
        - North_Bound (eth0) of Master Director, Controller_1, Controller_2, Analytics_1, Analytics_2, Analytics_3 and Analytics_4 should be reachable from Slave Director.
        - South_Bound (eth1) of Analytics_1, Analytics_2, Analytics_3, Analytics_4 should be reachable from Slave Director via south_bound(eth1).

Topology

Usage

    - Download the below files in Master Director, is located "https://”,
    - It will require below files to run the script.

        |____analytics_cluster_conf
        |____controller_data.json
        |____dc_dr_network_config
        |____he_automation_21_2_1.py
        |____he_automation.csv
        |____he_automation.py

script execution syntax:


    --without Service_vnf
        syntax :
            python3 he_automation.py -vdu <director username> -pwd <password> -with_out_flex

                -vdu/--username     director username
                -pwd/--password     director password
                -with_out_flex/--with_out_flex      configure the head end components with_out flex vnf configuration

    --with Service_vnf
        syntax :
            python3 he_automation.py -vdu <director username> -pwd <password> -with_flex

                -vdu/--username     director username
                -pwd/--password     director password
                -with_out_flex/--with_out_flex      configure the head end components with flex vnf configuration
