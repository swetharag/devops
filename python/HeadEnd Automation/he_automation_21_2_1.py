# from jinja2.environment import Template
from pprint import pprint
import requests
import json
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
requests.packages.urllib3.disable_warnings()

he_automation_api = """{
    "director_service_status":"https://{}:{}/api/config/system/_operations/status?deep=true",
    "director_ha":"https://{}:{}/api/config/vnmsha/actions/_operations/enable-ha",
    "director_ha_status":"https://{}:{}/vnms/dashboard/vdStatus/haDetails",
    "user_creation":"https://{}:{}/api/config/nms/actions/_operations/create-user",
    "user_deletion":"https://{}:{}/api/config/nms/actions/_operations/delete-user",
    "device_creation":"https://{}:{}/nextgen/localorganization",
    "org_creation":"https://{}:{}/nextgen/organization",
    "organization_fetch":"https://{}:{}/vnms/sdwan/workflow/orgs/org/{{orgWorkflowName}}",
    "wan_network_creation":"https://{}:{}/nextgen/organization/{{org_uuid}}/wan-networks",
    "device_addition":"https://{}:{}/api/config/nms/actions/_operations/add-devices",
    "static_route_addition":"https://{}:{}/api/config/nms/routing-options/static",
    "overlay_address_fetch":"https://{}:{}/vnms/ipam/overlay",
    "overlay_address_deletion":"https://{}:{}/vnms/ipam/overlay/prefixes/1",
    "overlay_address_addition":"https://{}:{}/vnms/ipam/overlay/prefixes",
    "controller_save":"https://{}:{}/vnms/sdwan/workflow/controllers/controller",
    "controller_deploy":"https://{}:{}/vnms/sdwan/workflow/controllers/controller/deploy/{{controller_hostname}},
    "task_status":"https://{}:{}/vnms/tasks/task/{{task_id}}",
    "analytics_cluster_config":"https://{}:{}/api/config/nms/provider/analytics-cluster/{{cluster_name}}"
}"""
class he_automation_21_2_1():

    def __init__(self, director_ip, username, password,director_port=9182):
        self.director_ip = director_ip
        self.username = username
        self.password = password
        self.director_port = director_port        
        self.requests_exception = requests.exceptions.ConnectionError
        # self.requests_JSONDecodeError_exception = requests.JSONDecodeError

    # def gen_url(self,work_flow_url,
    #                             templateworkflowname=None,
    #                             deviceworkflowname=None,
    #                             devicegroupname=None,
    #                             controllerworkflowname=None,
    #                             device_uuid=None,
    #                             spack_version_number=None):
    #     # templateworkflowname=kwargs["templateworkflowname"]
    #     self.he_automation_api = Template(he_automation_api)
    #     self.he_automation_api_update = self.he_automation_api.render(
    #                                 templateworkflowname=templateworkflowname,
    #                                 deviceworkflowname=deviceworkflowname,
    #                                 devicegroupname=devicegroupname,
    #                                 controllerworkflowname=controllerworkflowname,
    #                                 device_uuid=device_uuid,
    #                                 spack_version_number=spack_version_number)
    #     self.he_automation_api = json.loads(self.he_automation_api_update)
    #     # print(self.he_automation_api)
    #     return self.he_automation_api[work_flow_url].format(self.director_ip, self.director_port)

    def request_get(self,url):
        try:
            response = requests.get(url,auth=(self.username, self.password), verify=False, timeout=(5, 120),
                            headers={'Accept': 'application/json'} )
            # print(json.dumps(response.json(), indent=4))
            # return(json.dumps(response.json(), indent=4))        
            return(response.json())
        except self.requests_exception as req_excep:
            print(req_excep)
        # except self.requests_JSONDecodeError_exception:
        #     response = requests.get(url,auth=(self.username, self.password), verify=False, timeout=(5, 120),
        #                     headers={'Accept': 'application/json'} )     
        #     return(response.text)                                   

    def request_post(self,url,json_data=None):
        try:
            # print(url)
            response = requests.post(url,auth=(self.username, self.password), verify=False, timeout=(5, 120),
                            headers={'Accept': 'application/json'},json = json_data)
            # print(response.json())
            # print(json.dumps(response.json(), indent=4))
            # return(json.dumps(response.json(), indent=4))        
            return(response.status_code)
            # return(response)
        except self.requests_exception as req_excep:
            print(req_excep)

    def request_post_json(self,url,json_data=None):
        try:
            # print(url)
            response = requests.post(url,auth=(self.username, self.password), verify=False, timeout=(5, 120),
                            headers={'Accept': 'application/json'},json = json_data)
            if (response.status_code) == 200:
                return(response.json())
            if (response.status_code) == 202:
                return(response.json())                
            if (response.status_code) == 400:
                return(response.json())                
            else:
                return(response)
        except self.requests_exception as req_excep:
            # print(req_excep)
            return(req_excep)

    def request_put(self,url,json_data=None):
        try:
            response = requests.put(url,auth=(self.username, self.password), verify=False, timeout=(5, 120),
                            headers={'Accept': 'application/json'},json = json_data)
            # print(json.dumps(response.json(), indent=4))
            # return(json.dumps(response.json(), indent=4))        
            return(response.status_code)
        except self.requests_exception as req_excep:
            print(req_excep)

    def request_delete(self,url):
        try:
            response = requests.delete(url,auth=(self.username, self.password), verify=False, timeout=(5, 120),
                            headers={'Accept': 'application/json'})
            # print(json.dumps(response.json(), indent=4))
            # return(json.dumps(response.json(), indent=4))        
            return(response.status_code)
        except self.requests_exception as req_excep:
            print(req_excep)


    def director_ha(self,ha_config_data):
        director_ha = 'https://{}:{}/api/config/vnmsha/actions/_operations/enable-ha'.format(self.director_ip,self.director_port)
        director_ha = self.request_post_json(director_ha.format(self.director_ip,self.director_port),ha_config_data)
        # user_creation = self.request_post_json(self.gen_url("user_creation"),user_creation_data)
        return(director_ha)

    def director_ha_status(self):
        director_ha_status = "https://{}:{}/vnms/dashboard/vdStatus/haDetails"
        director_ha_status = self.request_get(director_ha_status.format(self.director_ip,self.director_port))
        return(director_ha_status)
        
    def director_service_status(self):
        director_service_status = "https://{}:{}/api/config/system/_operations/status?deep=true"
        director_service_status = self.request_post_json(director_service_status.format(self.director_ip,self.director_port))
        return(director_service_status)

    def analytics_cluster_config(self,cluster_name,cluster_data):
        analytics_cluster_config = 'https://{}:{}/api/config/nms/provider/analytics-cluster/{}'.format(self.director_ip,self.director_port,cluster_name)
        analytics_cluster_config = self.request_put(analytics_cluster_config.format(self.director_ip,self.director_port),cluster_data)
        # user_creation = self.request_post_json(self.gen_url("user_creation"),user_creation_data)
        return(analytics_cluster_config)


    def task_status(self,task_id):
        task_status = "https://{}:{}/vnms/tasks/task/{}"
        task_status = self.request_get(task_status.format(self.director_ip,self.director_port,task_id))
        # task_status = self.request_get(self.gen_url("task_status",task_id=task_id))
        return(task_status)


    def user_creation(self,user_creation_data):
        user_creation = 'https://{}:{}/api/config/nms/actions/_operations/create-user'.format(self.director_ip,self.director_port)
        user_creation = self.request_post_json(user_creation.format(self.director_ip,self.director_port),user_creation_data)
        # user_creation = self.request_post_json(self.gen_url("user_creation"),user_creation_data)
        return(user_creation)

    def user_deletion(self,user_deletion_data):
        user_deletion = 'https://{}:{}/api/config/nms/actions/_operations/delete-user'.format(self.director_ip,self.director_port)
        user_deletion = self.request_post_json(user_deletion.format(self.director_ip,self.director_port),user_deletion_data)
        # user_creation = self.request_post_json(self.gen_url("user_deletion"),user_deletion_data)
        return(user_deletion)

    def device_creation(self,device_creation_data):
        device_creation = "https://{}:{}/nextgen/localorganization"
        device_creation = self.request_post(device_creation.format(self.director_ip,self.director_port),device_creation_data)
        # device_creation = self.request_post(self.gen_url("device_creation"),device_creation_data)
        return(device_creation)

    def org_creation(self,org_creation_data):
        org_creation = "https://{}:{}/nextgen/organization"
        org_creation = self.request_post(org_creation.format(self.director_ip,self.director_port),org_creation_data)
        # org_creation = self.request_post(self.gen_url("org_creation"),org_creation_data)
        return(org_creation)

    def overlay_address_fetch(self):
        overlay_address_fetch = "https://{}:{}/vnms/ipam/overlay"
        overlay_address_fetch = self.request_get(overlay_address_fetch.format(self.director_ip,self.director_port))
        # overlay_address_fetch = self.request_get(overlay_address_fetch.format(self.director_ip,self.director_port))
        return(overlay_address_fetch)

    def org_fetch(self,org_name):
        organization_fetch ="https://{}:{}/vnms/sdwan/workflow/orgs/org/{}"
        get_org_Uuid = "https://{}:{}/vnms/organization/orgs"
        organization_fetch = self.request_get(get_org_Uuid.format(self.director_ip,self.director_port))
        # organization_fetch = self.request_get(organization_fetch.format(self.director_ip,self.director_port))
        return(organization_fetch)

    def device_addition(self,device_addition_data):
        device_addition = "https://{}:{}/api/config/nms/actions/_operations/add-devices"
        # device_addition = self.request_post(device_addition.format(self.director_ip,self.director_port),device_addition_data)
        device_addition = self.request_post_json(device_addition.format(self.director_ip,self.director_port),device_addition_data)
        # device_addition = self.request_post(self.gen_url("device_addition"),device_addition_data)
        return(device_addition)

    def static_route_addition(self,static_route_data):
        static_route_addition = "https://{}:{}/api/config/nms/routing-options/static"
        # static_route_addition = self.request_post_json(static_route_addition.format(self.director_ip,self.director_port),static_route_data)
        static_route_addition = self.request_post(static_route_addition.format(self.director_ip,self.director_port),static_route_data)
        # static_route_addition = self.request_post(self.gen_url("static_route_addition"),static_route_data)
        return(static_route_addition)

    def overlay_address_addition(self,overlay_address_data):
        overlay_address_addition = "https://{}:{}/vnms/ipam/overlay/prefixes"
        # overlay_address_addition = self.request_post_json(overlay_address_addition.format(self.director_ip,self.director_port),overlay_address_data)
        overlay_address_addition = self.request_post(overlay_address_addition.format(self.director_ip,self.director_port),overlay_address_data)
        # overlay_address_addition = self.request_post(self.gen_url("overlay_address_addition"),overlay_address_data)
        return(overlay_address_addition)

    def overlay_address_deletion(self):
        overlay_address_deletion = "https://{}:{}/vnms/ipam/overlay/prefixes/1"
        overlay_address_deletion = self.request_delete(overlay_address_deletion.format(self.director_ip,self.director_port))
        # overlay_address_deletion = self.request_delete(self.gen_url("overlay_address_deletion"))
        return(overlay_address_deletion)

    def wan_network(self,org_uuid,wan_network_data):
        wan_network_creation = "https://{}:{}/nextgen/organization/{}/wan-networks"
        wan_network_creation = self.request_post(wan_network_creation.format(self.director_ip,self.director_port,org_uuid),wan_network_data)
        return(wan_network_creation)

    def controller_save(self,controller_data):
        controller_save = "https://{}:{}/vnms/sdwan/workflow/controllers/controller"
        # controller_save = self.request_post_json(controller_save.format(self.director_ip,self.director_port),controller_data)
        controller_save = self.request_post(controller_save.format(self.director_ip,self.director_port),controller_data)
        # controller_save = self.request_post(self.gen_url("controller_save"),controller_data)
        return(controller_save)

    def controller_deploy(self,controller_hostname):
        controller_deploy = "https://{}:{}/vnms/sdwan/workflow/controllers/controller/deploy/{}"
        # controller_deploy = self.request_post_json(controller_deploy.format(self.director_ip,self.director_port,controller_hostname))
        controller_deploy = self.request_post_json(controller_deploy.format(self.director_ip,self.director_port,controller_hostname))
        # controller_deploy = self.request_post(self.gen_url("controller_deploy"),controller_hostname)
        return(controller_deploy)
            
if __name__ == '__main__':
    he_automation_21_2_1()
