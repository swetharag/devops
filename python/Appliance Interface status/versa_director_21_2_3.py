from jinja2.environment import Template
import requests
import json
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
requests.packages.urllib3.disable_warnings()

api_list = """{
    "director_package_info":"https://{}:{}/api/operational/system/package-info?deep=true",
    "appliance_all_info":"https://{}:{}/vnms/appliance/appliance?offset=0&limit=5000",
    "device_interface_status":"https://{}:{}/vnms/dashboard/appliance/{{deviceworkflowname}}/interfaces/deep?tenantName={{org_name}}"
}"""

class director_21_2_3():

    def __init__(self, director_ip, username, password,director_port=9182):
        self.director_ip = director_ip
        self.username = username
        self.password = password
        self.director_port = director_port   
        self.requests_exception = requests.exceptions.ConnectionError
        self.requests_JSONDecodeError_exception = requests.JSONDecodeError

    def gen_url(self,work_flow_url,
                                deviceworkflowname=None,
                                org_name=None):
        self.api_list = Template(api_list)
        self.api_list_update = self.api_list.render(
                                    deviceworkflowname=deviceworkflowname,
                                    org_name=org_name)
        self.api_list = json.loads(self.api_list_update)
        return self.api_list[work_flow_url].format(self.director_ip, self.director_port)

    def request_get(self,url):
        try:
            response = requests.get(url,auth=(self.username, self.password), verify=False, timeout=(5, 120),
                            headers={'Accept': 'application/json'} )
            if (response.status_code) == 204:
                return(response.status_code)
            else:            
                return(response.json())
        except self.requests_exception as req_excep:
            print(req_excep)
        except self.requests_JSONDecodeError_exception:
            response = requests.get(url,auth=(self.username, self.password), verify=False, timeout=(5, 120),
                            headers={'Accept': 'application/json'} )     
            if (response.status_code) == 204:
                return(response.status_code)
            if (response.status_code) == 404:
                return(response.status_code)
            else:                               
                return(response.text)                                   

    def director_package_info(self):
        director_package_info = self.request_get(self.gen_url("director_package_info"))
        return(director_package_info)
        
    def appliance_all_info(self):
        appliance_all_info = self.request_get(self.gen_url("appliance_all_info"))
        return(appliance_all_info)
    
    def device_interface_status(self,deviceworkflowname,org_name):
        device_interface_status = self.request_get(self.gen_url("device_interface_status",deviceworkflowname=deviceworkflowname,org_name=org_name))
        return(device_interface_status)
            
if __name__ == '__main__':
    director_21_2_3()
