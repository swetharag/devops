Version

	v.1.0
    
Summary

    This script will help us to fetch the "REACHABLE" appliance interface status details from director. After the successful execution, the script will generate the output in interface_detail_status.csv file.

Prerequisites

	-- Python 3.8 and above
	-- Connectivity to Director
	-- Admin credentials for GUI.
	-- lib
		-- Jinja2==3.0.1
		-- pandas==1.3.3
		-- requests==2.27.1
		-- urllib3==1.26.3

Usage

    - Download the below files, is located "https://”,
    - It will require below files to run the script.

        |____ versa_director_21_2_3.py
        |____ interface_up_down.py

library installation:
    pip install -r requirements.txt

script execution:
        syntax :
            python3 interface_up_down.py -vd <director_ip> -vdu <director username> -pwd <password>

                -vd/--director_ip   director ip
                -vdu/--username     director username
                -pwd/--password     director password

        logs:

            (mac_venv) dinz@Dineshkumars-MacBook-Pro interface_up_down % python interface_up_down.py -vd 10.192.130.2 -vdu Administrator -pwd Versa@1234
            2022-10-21 16:52:57,548 [INFO] Getting All The Device Details From The Director
            2022-10-21 16:52:57,703 [INFO] Checking Device Interface Status For chennai-branch
            2022-10-21 16:52:57,877 [INFO] Checking Device Interface Status For Controller1
            2022-10-21 16:52:58,039 [INFO] Checking Device Interface Status For Controller2
                        name  ping-status sync-status services-status   ownerOrg   Interface Interface_Type Oper_Status Admin_Status AdminUp OperUp
            0     BLR-CON-CPE1  UNREACHABLE     UNKNOWN         UNKNOWN  IOCL-TEST         NaN            NaN         NaN          NaN     NaN    NaN
            1   chennai-branch    REACHABLE     IN_SYNC            GOOD  IOCL-TEST         NaN            NaN         NaN          NaN     NaN    NaN
            2   chennai-branch    REACHABLE     IN_SYNC            GOOD  IOCL-TEST  tvi-0/99.0            lan          up           up    True   True
            3   chennai-branch    REACHABLE     IN_SYNC            GOOD  IOCL-TEST   vni-0/0.0            wan          up           up    True   True
            4   chennai-branch    REACHABLE     IN_SYNC            GOOD  IOCL-TEST   vni-0/1.0            wan          up           up    True   True
            5   chennai-branch    REACHABLE     IN_SYNC            GOOD  IOCL-TEST   vni-0/2.0            lan          up           up    True   True
            6   chennai-branch    REACHABLE     IN_SYNC            GOOD  IOCL-TEST   vni-0/3.0            lan          up           up    True   True
            7   chennai-branch    REACHABLE     IN_SYNC            GOOD  IOCL-TEST   vni-0/4.0            lan          up           up    True   True
            8      Controller1    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/4.0            lan          up           up    True   True
            9      Controller1    REACHABLE     IN_SYNC            GOOD      VERSA  tvi-1/99.0            lan          up           up    True   True
            10     Controller1    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/0.0            lan          up           up    True   True
            11     Controller1    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/1.0            wan          up           up    True   True
            12     Controller1    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/2.0            wan          up           up    True   True
            13     Controller1    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/3.0            wan          up           up    True   True
            14     Controller2    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/3.0            wan          up           up    True   True
            15     Controller2    REACHABLE     IN_SYNC            GOOD      VERSA  tvi-1/99.0            lan          up           up    True   True
            16     Controller2    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/0.0            lan          up           up    True   True
            17     Controller2    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/1.0            wan          up           up    True   True
            18     Controller2    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/2.0            wan          up           up    True   True
            19     Controller2    REACHABLE     IN_SYNC            GOOD      VERSA   vni-0/3.0            wan          up           up    True   True
            (mac_venv) dinz@Dineshkumars-MacBook-Pro interface_up_down % 
        
        csv output:

            (mac_venv) dinz@Dineshkumars-MacBook-Pro interface_up_down % cat interface_detail_status.csv| column -s, -t
            name            ping-status  sync-status  services-status  ownerOrg   Interface   Interface_Type  Oper_Status  Admin_Status  AdminUp  OperUp
            BLR-CON-CPE1    UNREACHABLE  UNKNOWN      UNKNOWN          IOCL-TEST
            chennai-branch  REACHABLE    IN_SYNC      GOOD             IOCL-TEST
            chennai-branch  REACHABLE    IN_SYNC      GOOD             IOCL-TEST  tvi-0/99.0  lan             up           up            True     True
            chennai-branch  REACHABLE    IN_SYNC      GOOD             IOCL-TEST  vni-0/0.0   wan             up           up            True     True
            chennai-branch  REACHABLE    IN_SYNC      GOOD             IOCL-TEST  vni-0/1.0   wan             up           up            True     True
            chennai-branch  REACHABLE    IN_SYNC      GOOD             IOCL-TEST  vni-0/2.0   lan             up           up            True     True
            chennai-branch  REACHABLE    IN_SYNC      GOOD             IOCL-TEST  vni-0/3.0   lan             up           up            True     True
            chennai-branch  REACHABLE    IN_SYNC      GOOD             IOCL-TEST  vni-0/4.0   lan             up           up            True     True
            Controller1     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/4.0   lan             up           up            True     True
            Controller1     REACHABLE    IN_SYNC      GOOD             VERSA      tvi-1/99.0  lan             up           up            True     True
            Controller1     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/0.0   lan             up           up            True     True
            Controller1     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/1.0   wan             up           up            True     True
            Controller1     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/2.0   wan             up           up            True     True
            Controller1     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/3.0   wan             up           up            True     True
            Controller2     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/3.0   wan             up           up            True     True
            Controller2     REACHABLE    IN_SYNC      GOOD             VERSA      tvi-1/99.0  lan             up           up            True     True
            Controller2     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/0.0   lan             up           up            True     True
            Controller2     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/1.0   wan             up           up            True     True
            Controller2     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/2.0   wan             up           up            True     True
            Controller2     REACHABLE    IN_SYNC      GOOD             VERSA      vni-0/3.0   wan             up           up            True     True
            (mac_venv) dinz@Dineshkumars-MacBook-Pro interface_up_down % 