#! /usr/bin/env python3
import os
from datetime import datetime
from queue import Empty
import time,sys
import logging
import json
import argparse
import paramiko
import time
import subprocess
import uuid
import csv
import ipaddress
from he_automation_21_2_1 import he_automation_21_2_1

true = "true"
false = "false"

class full_automaton():

    def __init__(self, **kwargs):
        parser = self.get_args()
        args = parser.parse_args()        
        self.log_setup()
        self.he_component_status_ = []
        self.he_component_status_dict = {}
        self.username = args.username
        self.password = args.password
        self.with_flex = args.with_flex
        self.with_out_flex = args.with_out_flex
        logging.info('HE Automation Script Initiated')
        logging.info('Reading csv file'.title())
        # reading csv file
        with open('he_automation.csv', 'r',encoding='utf-8-sig') as file:
            # reader = csv.reader(file, delimiter = '\t')
            reader = csv.reader(file)
            dic = {}
            for row in reader:
                # print(row)
                c1,c2,c3,c4 = row
                dic[c1] = c2
                dic[c3] = c4

        self.parent_org_name = dic["parent_org_name"].strip()
        self.van_cluster_name = dic["van_cluster_name"].strip()
        
        self.wan1_transport_domain = dic["wan1_transport_domain"].strip()
        self.wan2_transport_domain = dic["wan2_transport_domain"].strip()

        self.overlay_subnet = ipaddress.IPv4Interface(dic["overlay_subnet"].strip()).network
        self.dc_management_subnet = ipaddress.IPv4Interface(dic["dc_management_subnet"].strip()).network
        self.dc_southbound_subnet = ipaddress.IPv4Interface(dic["dc_southbound_subnet"].strip()).network
        self.dc_controll_subnet = ipaddress.IPv4Interface(dic["dc_controll_subnet"].strip()).network
        self.dc_internet_subnet = ipaddress.IPv4Interface(dic["dc_internet_subnet"].strip()).network
        self.dc_mpls_subnet = ipaddress.IPv4Interface(dic["dc_mpls_subnet"].strip()).network
        self.dc_interconnect_subnet = ipaddress.IPv4Interface(dic["dc_interconnect_subnet"].strip()).network

        self.dr_management_subnet = ipaddress.IPv4Interface(dic["dr_management_subnet"].strip()).network
        self.dr_southbound_subnet = ipaddress.IPv4Interface(dic["dr_southbound_subnet"].strip()).network
        self.dr_controll_subnet = ipaddress.IPv4Interface(dic["dr_controll_subnet"].strip()).network
        self.dr_internet_subnet = ipaddress.IPv4Interface(dic["dr_internet_subnet"].strip()).network
        self.dr_mpls_subnet = ipaddress.IPv4Interface(dic["dr_mpls_subnet"].strip()).network
        self.dr_interconnect_subnet = ipaddress.IPv4Interface(dic["dr_interconnect_subnet"].strip()).network

        self.master_dir_host_name = dic["master_dir_host_name"].strip()
        self.master_dir_mgmt_ip_eth0=dic["master_dir_mgmt_ip_eth0"].strip()
        self.master_dir_south_bound_ip_eth1=dic["master_dir_south_bound_ip_eth1"].strip()        

        self.slave_dir_host_name = dic["slave_dir_host_name"].strip()
        self.slave_dir_mgmt_ip_eth0=dic["slave_dir_mgmt_ip_eth0"].strip()
        self.slave_dir_south_bound_ip_eth1= dic["slave_dir_south_bound_ip_eth1"].strip()

        
        self.dc_service_vnf_name=dic["dc_service_vnf_name"].strip()
        self.dc_service_vnf_mgnt_ip_eth0=dic["dc_service_vnf_mgnt_ip_eth0"].strip()
        self.dc_service_vnf_towards_director_eth1=dic["dc_service_vnf_towards_director_eth1"].strip()
        self.dc_service_vnf_towards_dr_eth2=dic["dc_service_vnf_towards_dr_eth2"].strip()
        self.dc_service_vnf_towards_dr_eth2_gateway=dic["dc_service_vnf_towards_dr_eth2_gateway"].strip()
        self.dc_service_vnf_towards_controller_eth3=dic["dc_service_vnf_towards_controller_eth3"].strip()

        self.dr_service_vnf_name=dic["dr_service_vnf_name"].strip()
        self.dr_service_vnf_mgnt_ip_eth0=dic["dr_service_vnf_mgnt_ip_eth0"].strip()
        self.dr_service_vnf_towards_director_eth1=dic["dr_service_vnf_towards_director_eth1"].strip()
        self.dr_service_vnf_towards_dc_eth2=dic["dr_service_vnf_towards_dc_eth2"].strip()
        self.dr_service_vnf_towards_dc_eth2_gateway=dic["dr_service_vnf_towards_dc_eth2_gateway"].strip()
        self.dr_service_vnf_towards_controller_eth3=dic["dr_service_vnf_towards_controller_eth3"].strip()
        #Controller 1 Information
        self.controller_1_hostname=dic["controller_1_hostname"].strip()
        self.controller_1_country= dic["controller_1_country"].strip()
        self.controller_1_mgnt_ip_eth0= dic["controller_1_mgnt_ip_eth0"].strip()
        self.controller_1_controll_network_ip_eth1=dic["controller_1_controll_network_ip_eth1"].strip()
        self.controller_1_wan_1_ip_eth2 = dic["controller_1_wan_1_ip_eth2"].strip()
        self.controller_1_wan_1_ip_eth2_gateway = dic["controller_1_wan_1_ip_eth2_gateway"].strip()
        self.controller_1_wan_2_ip_eth3= dic["controller_1_wan_2_ip_eth3"].strip()
        self.controller_1_wan_2_ip_eth3_gateway= dic["controller_1_wan_2_ip_eth3_gateway"].strip()
        self.controller_1_internet_public_ip=dic["controller_1_public_ip"].strip()
        #Controller 2 Information
        self.controller_2_hostname=dic["controller_2_hostname"].strip()
        self.controller_2_country= dic["controller_2_country"].strip()
        self.controller_2_mgnt_ip_eth0= dic["controller_2_mgnt_ip_eth0"].strip()
        self.controller_2_controll_network_ip_eth1=dic["controller_2_controll_network_ip_eth1"].strip()
        self.controller_2_wan_1_ip_eth2= dic["controller_2_wan_1_ip_eth2"].strip()
        self.controller_2_wan_1_ip_eth2_gateway= dic["controller_2_wan_1_ip_eth2_gateway"].strip()
        self.controller_2_wan_2_ip_eth3= dic["controller_2_wan_2_ip_eth3"].strip()
        self.controller_2_wan_2_ip_eth3_gateway= dic["controller_2_wan_2_ip_eth3_gateway"].strip()
        self.controller_2_internet_public_ip=dic["controller_2_public_ip"].strip()

        #Analytics_info

        self.van_cluster_name = dic["van_cluster_name"].strip()
        self.analytics_1_hostname = dic["analytics_1_hostname"].strip()
        self.analytics_1_mgnt_ip_eth0 = dic["analytics_1_mgnt_ip_eth0"].strip()
        self.analytics_1_south_bound_ip_eth1 = dic["analytics_1_south_bound_ip_eth1"].strip()
        self.analytics_2_hostname = dic["analytics_2_hostname"].strip()
        self.analytics_2_mgnt_ip_eth0 = dic["analytics_2_mgnt_ip_eth0"].strip()
        self.analytics_2_south_bound_ip_eth1 = dic["analytics_2_south_bound_ip_eth1"].strip()
        self.search_1_hostname = dic["search_1_hostname"].strip()
        self.search_1_mgnt_ip_eth0 = dic["search_1_mgnt_ip_eth0"].strip()
        self.search_1_south_bound_ip_eth1 = dic["search_1_south_bound_ip_eth1"].strip()
        self.search_2_hostname = dic["search_2_hostname"].strip()
        self.search_2_mgnt_ip_eth0 = dic["search_2_mgnt_ip_eth0"].strip()
        self.search_2_south_bound_ip_eth1 = dic["search_2_south_bound_ip_eth1"].strip()

        # self.vd = he_automation_21_2_1(self.master_dir_mgmt_ip_eth0,"Administrator", password="Versa@123")
        # self.vd = he_automation_21_2_1(self.master_dir_mgmt_ip_eth0,"Administrator", password="versa123")
        # self.slave_vd = he_automation_21_2_1(self.slave_dir_mgmt_ip_eth0,"Administrator", password="versa123")
        self.vd = he_automation_21_2_1(self.master_dir_mgmt_ip_eth0,self.username, self.password)
        self.slave_vd = he_automation_21_2_1(self.slave_dir_mgmt_ip_eth0,self.username, self.password)
        self.vd_automation = he_automation_21_2_1(self.master_dir_mgmt_ip_eth0,"Administrator", password="versa123")

    def get_args(self):
        parser = argparse.ArgumentParser(description='HeadEnd Automation')

        parser.add_argument('-vdu', '--username',required=True,
                            help="Director Username")
        parser.add_argument('-pwd', '--password',required=True,
                            help="Director Password")
        parser.add_argument('-with_flex', '--with_flex',action="store_true",
                            help="With flex VNF deployment")
        parser.add_argument('-with_out_flex', '--with_out_flex',action="store_true",
                            help="With out flex VNF deployment")                         
        return parser    

    def ip_validation(self):
        logging.info("ip address validation")
        # print(self.dc_internet_subnet)
        # print(ipaddress.IPv4Interface(self.dc_management_subnet).ip)
        # print(ipaddress.IPv4Network(self.dc_management_subnet).prefixlen)
        controll_subnet_prefixlen = ipaddress.IPv4Network(self.dc_controll_subnet).prefixlen
        print(self.controller_1_controll_network_ip_eth1+"/"+str(controll_subnet_prefixlen))
        # print(self.controller_1_controll_network_ip_eth1+"/"+ipaddress.IPv4Network(self.dc_controll_subnet).prefixlen)
        
        # if ipaddress.IPv4Interface(self.dc_management_subnet).network == self.dc_management_subnet:
        #     print("Subnet is correcr")
        # else:
        #     print("Subnet is wrong")
        # test = ipaddress.IPv4Interface(self.dc_management_subnet)
        # print(ipaddress.IPv4Interface(self.dc_management_subnet).ip)
        # print(ipaddress.IPv4Interface(self.dc_management_subnet).network)
        # print(test.ip)
        # print(test.network)
        # print(test.with_prefixlen)

    def log_setup(self):      

        file_handler = logging.FileHandler("he_automation.log")
        console_handler = logging.StreamHandler(sys.stdout)
        handelers = [file_handler, console_handler]
        logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s", handlers=handelers)
        self.remove_file()

    def remove_file(self):
        if os.path.exists('slave_director_ping_check_script_output'):
            os.system("rm -rf slave_director_ping_check_script_output")
        else:
            pass


    def service_vnf_rechability_check(self):
        logging.info('service vnf Reachability check'.title())
        service_vnf_mgnt_ip = {"dc_service_vnf_mgnt_ip_eth0":self.dc_service_vnf_mgnt_ip_eth0,
                                        "dr_service_vnf_mgnt_ip_eth0":self.dr_service_vnf_mgnt_ip_eth0} 
        logging.info('Checking service vnf Reachability from Master Director'.title())                  
        for device_ip in service_vnf_mgnt_ip:
            self.ping_check(device_ip, service_vnf_mgnt_ip[device_ip])       
        for status in self.he_component_status_:
            if status["status"] == "Down":
                logging.error('Check the connectivity for sevice vnf from Master Director!'.title())
                exit()

    def service_vnf_1(self):
        logging.info("configuring service vnf 1".title())
        dc_dr_config = open('dc_dr_network_config')
        dc_dr_config = dc_dr_config.read()
        #service_vnf 1 data update        
        South_Bound_Network_ip_prefixlen = ipaddress.IPv4Network(self.dc_southbound_subnet).prefixlen
        dc_dr_interconnect_local_prefixlen = ipaddress.IPv4Network(self.dc_interconnect_subnet).prefixlen
        router_to_controller_prefixlen = ipaddress.IPv4Network(self.dc_controll_subnet).prefixlen
        South_Bound_Network_ip_with_prefixlen = self.dc_service_vnf_towards_director_eth1+"/"+str(South_Bound_Network_ip_prefixlen)
        dc_dr_interconnect_local_with_prefixlen = self.dc_service_vnf_towards_dr_eth2+"/"+str(dc_dr_interconnect_local_prefixlen)
        router_to_controller_with_prefixlen = self.dc_service_vnf_towards_controller_eth3+"/"+str(router_to_controller_prefixlen)
        checkWords = ("South_Bound_Network_ip","sb_local_ip","interconnect_local_ip","router_to_controller","local_routertocontroller_ip","ipsec_interconnect_ip","ipsec_ip_remote","ipsec_ip_local","parent_org_name","dc_dr_interconnect_local","dc_dr_interconnect_remote","dc_dr_interconnect_gateway","controller_1_south_bound_ip","DC_mgmt_subnet","South_Bound_Network_gateway")
        repWords = (South_Bound_Network_ip_with_prefixlen,self.dc_service_vnf_towards_director_eth1,dc_dr_interconnect_local_with_prefixlen,router_to_controller_with_prefixlen,self.dc_service_vnf_towards_controller_eth3,"192.168.0.1/30","192.168.0.2","192.168.0.1",self.parent_org_name,self.dc_service_vnf_towards_dr_eth2,self.dr_service_vnf_towards_dc_eth2,self.dc_service_vnf_towards_dr_eth2_gateway,self.controller_1_controll_network_ip_eth1,str(self.dc_management_subnet),self.master_dir_south_bound_ip_eth1)
        #for each line in the input file
        dc_dr_config_update = open("dc_dr_network_config_update.cfg", "wt")        
        for line in dc_dr_config.splitlines():
            # read replace the string and write to output file
            for check, rep in zip(checkWords, repWords):
                line = line.replace(check, rep)
            dc_dr_config_update.write(line)
            dc_dr_config_update.write('\n')
        dc_dr_config_update.close()
        os.system("sshpass -p 'versa123' scp -o StrictHostKeyChecking=no dc_dr_network_config_update.cfg admin@"+self.dc_service_vnf_mgnt_ip_eth0+":/tmp/dc_dr_network_config_update.cfg")
        client = paramiko.client.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(self.dc_service_vnf_mgnt_ip_eth0, username='admin', password='versa123') 
        _stdin, _stdout,_stderr = client.exec_command("echo -e 'configure\nload merge /tmp/dc_dr_network_config_update.cfg\ncommit' | /opt/versa/confd/bin/confd_cli -u admin -g admin")
        # _stdin, _stdout,_stderr = client.exec_command("ls -l")
        time.sleep(5)
        logging.info(_stdout.read().decode())
        client.close()
        os.system("rm -rf dc_dr_network_config_update.cfg") 
        self.service_vnf_2()

    def service_vnf_2(self):
        logging.info("configuring service vnf 2".title())
        dr_dc_config = open('dc_dr_network_config')
        dr_dc_config = dr_dc_config.read()
        #service_vnf 2 data update        
        South_Bound_Network_ip_prefixlen = ipaddress.IPv4Network(self.dr_southbound_subnet).prefixlen
        dc_dr_interconnect_local_prefixlen = ipaddress.IPv4Network(self.dr_interconnect_subnet).prefixlen
        router_to_controller_prefixlen = ipaddress.IPv4Network(self.dr_controll_subnet).prefixlen
        South_Bound_Network_ip_with_prefixlen = self.dr_service_vnf_towards_director_eth1+"/"+str(South_Bound_Network_ip_prefixlen)
        dc_dr_interconnect_local_with_prefixlen = self.dr_service_vnf_towards_dc_eth2+"/"+str(dc_dr_interconnect_local_prefixlen)
        router_to_controller_with_prefixlen = self.dr_service_vnf_towards_controller_eth3+"/"+str(router_to_controller_prefixlen)
        checkWords = ("South_Bound_Network_ip","sb_local_ip","interconnect_local_ip","router_to_controller","local_routertocontroller_ip","ipsec_interconnect_ip","ipsec_ip_remote","ipsec_ip_local","parent_org_name","dc_dr_interconnect_local","dc_dr_interconnect_remote","dc_dr_interconnect_gateway","controller_1_south_bound_ip","DC_mgmt_subnet","South_Bound_Network_gateway")
        repWords = (South_Bound_Network_ip_with_prefixlen,self.dr_service_vnf_towards_director_eth1,dc_dr_interconnect_local_with_prefixlen,router_to_controller_with_prefixlen,self.dr_service_vnf_towards_controller_eth3,"192.168.0.2/30","192.168.0.1","192.168.0.2",self.parent_org_name,self.dr_service_vnf_towards_dc_eth2,self.dc_service_vnf_towards_dr_eth2,self.dr_service_vnf_towards_dc_eth2_gateway,self.controller_2_controll_network_ip_eth1,str(self.dr_management_subnet),self.slave_dir_south_bound_ip_eth1)
        dr_dc_config_update = open("dr_dc_network_config_update.cfg", "wt")        
        for line in dr_dc_config.splitlines():
            # read replace the string and write to output file
            for check, rep in zip(checkWords, repWords):
                line = line.replace(check, rep)
            dr_dc_config_update.write(line)
            dr_dc_config_update.write('\n')
        dr_dc_config_update.close()
        os.system("sshpass -p 'versa123' scp -o StrictHostKeyChecking=no dr_dc_network_config_update.cfg admin@"+self.dr_service_vnf_mgnt_ip_eth0+":/tmp/dr_dc_network_config_update.cfg")
        client = paramiko.client.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(self.dr_service_vnf_mgnt_ip_eth0, username='admin', password='versa123') 
        _stdin, _stdout,_stderr = client.exec_command("echo -e 'configure\nload merge /tmp/dr_dc_network_config_update.cfg\ncommit' | /opt/versa/confd/bin/confd_cli -u admin -g admin")
        # _stdin, _stdout,_stderr = client.exec_command("ls -l")
        time.sleep(5)
        logging.info(_stdout.read().decode())
        client.close()
        os.system("rm -rf dr_dc_network_config_update.cfg")
        
    def rechability_check(self):
        logging.info('HE components Reachability check'.title())
        he_components_north_bound_ips = {"Master_director_eth0":self.master_dir_mgmt_ip_eth0,
                                        "slave_director_eth0":self.slave_dir_mgmt_ip_eth0,
                                        "controller_1_eth0":self.controller_1_mgnt_ip_eth0,
                                        "controller_2_eth0":self.controller_2_mgnt_ip_eth0,
                                        "analytics_1_eth0":self.analytics_1_mgnt_ip_eth0,
                                        "analytics_2_eth0":self.analytics_2_mgnt_ip_eth0,
                                        "search_1_eth0":self.search_1_mgnt_ip_eth0,
                                        "search_2_eth0":self.search_2_mgnt_ip_eth0}   
        he_components_south_bound_ips = {"analytics_1_eth1":self.analytics_1_south_bound_ip_eth1,
                                        "analytics_2_eth1":self.analytics_2_south_bound_ip_eth1,
                                        "search_1_eth1":self.search_1_south_bound_ip_eth1,
                                        "search_2_eth1":self.search_2_south_bound_ip_eth1}    
        logging.info('Checking NorthBound Reachability from Master Director'.title())                  
        for device_ip in he_components_north_bound_ips:
            self.ping_check(device_ip, he_components_north_bound_ips[device_ip])       
        logging.info('Checking SouthBound Reachability from Master Director'.title())                    
        for device_ip in he_components_south_bound_ips:
            self.ping_check(device_ip, he_components_south_bound_ips[device_ip])
        for status in self.he_component_status_:
            if status["status"] == "Down":
                logging.error('Check the connectivity between Master Director to HE Components !'.title())
                exit()
        self.slave_director_ping_check_script(str(he_components_north_bound_ips),str(he_components_south_bound_ips))

    def slave_dir_file_check(self):
        # logging.info('Checking slave director ping check script output'.title())
        if os.path.exists('slave_director_ping_check_script_output'):
            return True
        else:
            return False


    def slave_director_ping_check_script(self,he_components_north_bound_ips,he_components_south_bound_ips):
        slave_director_ping_check_script_py_data = """
he_component_status_ = []
he_component_status_dict = {}        
def ping_check(device,ip):    
    proc = subprocess.Popen(['ping', '-c', '3', ip],stdout=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    if proc.returncode == 0:
        #print('{}:{} is UP and reachable!'.format(device,ip.strip()))
        # logging.info('{}:{} is UP and reachable!'.format(device,ip.strip()))
        he_component_status_dict["device"] = device
        he_component_status_dict["IP"] = ip
        he_component_status_dict["status"] = "UP"
        he_component_status_.append(he_component_status_dict.copy())
    elif proc.returncode == 2 or 256 or 512:
        #print('{}:{} is DOWN and No response!'.format(device,ip.strip()))
        # logging.error('{}:{} is DOWN and No response!'.format(device,ip.strip()))
        he_component_status_dict["device"] = device
        he_component_status_dict["IP"] = ip
        he_component_status_dict["status"] = "Down"            
        he_component_status_.append(he_component_status_dict.copy())
    else:
        #print('{}:{} is DOWN and Host Unreachable!'.format(device,ip.strip()))
        # logging.error('{}:{} is DOWN and Host Unreachable!'.format(device,ip.strip()))
        he_component_status_dict["device"] = device
        he_component_status_dict["IP"] = ip
        he_component_status_dict["status"] = "Down"
        he_component_status_.append(he_component_status_dict.copy())
#print('Checking NorthBound Reachability from Slave Director'.title())                  
for device in he_components_north_bound_ips:
    ping_check(device, he_components_north_bound_ips[device])   
#print('Checking SouthBound Reachability from Slave Director'.title())                  
for device in he_components_south_bound_ips:
    ping_check(device, he_components_south_bound_ips[device])    
slave_director_ping_check_script_output = open("slave_director_ping_check_script_output", "wt")        
slave_director_ping_check_script_output.write(str(he_component_status_))
slave_director_ping_check_script_output.close()
        """
        master_dir_ssh_scp = 'os.system("'+"sshpass -p 'versa123' scp -o StrictHostKeyChecking=no /home/admin/slave_director_ping_check_script_output admin@"+self.master_dir_mgmt_ip_eth0+":/home/admin/automation/"+'")'
        slave_director_ping_check_script_py = open("ping_check_script.py", "wt")        
        slave_director_ping_check_script_py.write("import subprocess")
        slave_director_ping_check_script_py.write('\n')
        slave_director_ping_check_script_py.write("import os")
        slave_director_ping_check_script_py.write('\n')
        slave_director_ping_check_script_py.write("he_components_north_bound_ips = ")
        slave_director_ping_check_script_py.write(he_components_north_bound_ips)
        slave_director_ping_check_script_py.write('\n')
        slave_director_ping_check_script_py.write("he_components_south_bound_ips = ")        
        slave_director_ping_check_script_py.write(he_components_south_bound_ips)
        slave_director_ping_check_script_py.write('\n')
        slave_director_ping_check_script_py.write(slave_director_ping_check_script_py_data)
        slave_director_ping_check_script_py.write('\n')
        slave_director_ping_check_script_py.write(master_dir_ssh_scp)
        slave_director_ping_check_script_py.write('\n')
        slave_director_ping_check_script_py.close()     
        os.system("sshpass -p 'versa123' scp -o StrictHostKeyChecking=no ping_check_script.py admin@"+self.slave_dir_mgmt_ip_eth0+":/home/admin/ping_check_script.py")
        self.slave_dir_file_check_loop()
        self.slave_director_script_output_check()
      

    def slave_director_script_output_check(self):
        slave_director_script_output = open('slave_director_ping_check_script_output')
        slave_director_script_output = slave_director_script_output.read()
        slave_director_script_output = slave_director_script_output.replace("'",'"')
        slave_director_script_output = json.loads(slave_director_script_output)
        print("\n",end="")
        for status in slave_director_script_output:
            if status["status"] == "UP":
                logging.info('{}:{} is UP and reachable!'.format(status["device"],status["IP"]))
            elif status["status"] == "Down":
                logging.error('{}:{} is DOWN and Host Unreachable!'.format(status["device"],status["IP"]))
        for status in slave_director_script_output:   
            if status["status"] == "Down":             
                logging.error('Check the connectivity between Slave Director to HE Components !'.title())
                exit()            

    def slave_dir_file_check_loop(self):
        run = 1
        while self.slave_dir_file_check() == False:
            now = datetime.now()    
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S,%f")[:-3]
            total = 1007  # total number to reach
            bar_length = 50  # should be less than 100
            for i in range(total+1):
                percent = 100.0*i/total
                sys.stdout.write('\r')
                sys.stdout.write("{} [INFO] Checking Reachability from Slave Director [{:{}}]".format(dt_string,'='*int(percent/(100.0/bar_length)),bar_length))
                sys.stdout.flush()
                time.sleep(0.002)  
            if run == 1:
                self.slave_ssh_ping_check()
                run = run+1

    def slave_ssh_ping_check(self):
        print("\n",end="")
        client = paramiko.client.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(self.slave_dir_mgmt_ip_eth0, username='admin', password='versa123') 
        # self.slave_dir_file_check_loop()     
        _stdin, _stdout,_stderr = client.exec_command('python3 ping_check_script.py')
        # time.sleep(5)
        # print(_stdout.read().decode())
        client.close()
        os.system("rm -rf ping_check_script.py slave_director_ping_check_script_output")

    def ping_check(self,device,ip):
        proc = subprocess.Popen(['ping', '-c', '3', ip],stdout=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        if proc.returncode == 0:
            logging.info('{}:{} is UP and reachable!'.format(device,ip.strip()))
            self.he_component_status_dict["device"] = device
            self.he_component_status_dict["status"] = "UP"
            self.he_component_status_.append(self.he_component_status_dict.copy())
        elif proc.returncode == 2 or 256 or 512:
            logging.error('{}:{} is DOWN and No response!'.format(device,ip.strip()))
            self.he_component_status_dict["device"] = device
            self.he_component_status_dict["status"] = "Down"            
            self.he_component_status_.append(self.he_component_status_dict.copy())
        else:
            logging.error('{}:{} is DOWN and Host Unreachable!'.format(device,ip.strip()))
            self.he_component_status_dict["device"] = device
            self.he_component_status_dict["status"] = "Down"
            self.he_component_status_.append(self.he_component_status_dict.copy())

    def director_service_status(self):
        logging.info('Checking Master Director service status'.title())
        director_service_status = self.vd.director_service_status()
        if type(director_service_status) == dict:
            director_service_status = json.dumps(director_service_status)
            director_service_status = json.loads(director_service_status)
            logging.info("Master Director services is running".title())
            self.slave_director_service_status()
        else:
            logging.error("Master Director services is not running".title())
            exit()

    def slave_director_service_status(self):
        logging.info('Checking Slave Director service status'.title())
        director_service_status = self.slave_vd.director_service_status()
        if type(director_service_status) == dict:
            director_service_status = json.dumps(director_service_status)
            director_service_status = json.loads(director_service_status)
            logging.info("Slave Director services is running".title())
            self.director_ha()
        else:
            logging.error("Slave Director services is not running".title())
            exit()


    def director_ha(self):
        logging.info('Preparing Director HA configuration')

        ha_config = {
                        "enable-ha":
                        {
                            "mgmt-ip-address": "master_director_ip",
                            "peer-mgmt-ip-address": "slave_director_ip",
                            "designated-master-mgmt-ip-address": "master_director_ip",
                            "enable-auto-switchover": "false"
                        }
                    }

        ha_config_update = json.dumps(ha_config)
        ha_config_update = json.loads(ha_config_update)
        ha_config_update["enable-ha"]["mgmt-ip-address"] = self.master_dir_mgmt_ip_eth0
        ha_config_update["enable-ha"]["peer-mgmt-ip-address"] = self.slave_dir_mgmt_ip_eth0
        ha_config_update["enable-ha"]["designated-master-mgmt-ip-address"] = self.master_dir_mgmt_ip_eth0
        # ha_config_update["enable-ha"]["mgmt-ip-address"] = self.master_dir_south_bound_ip_eth1
        # ha_config_update["enable-ha"]["peer-mgmt-ip-address"] = self.slave_dir_south_bound_ip_eth1
        # ha_config_update["enable-ha"]["designated-master-mgmt-ip-address"] = self.master_dir_south_bound_ip_eth1
        logging.info('Director HA configuration started')
        dir_ha_config = json.dumps(self.vd.director_ha(ha_config_update))
        dir_ha_config = json.loads(dir_ha_config)
        dir_ha_config_task_id = dir_ha_config["output"]["result"]["task"]["task-id"]
        time.sleep(30)
        self.dir_task_status_monitor("Director Ha Configuration",dir_ha_config_task_id)
        time.sleep(20)
        self.director_ha_status()        

    def dir_task_status_monitor(self,custom_task_message,task_id):
        self.dir_task_status(task_id)
        task_in_progress = "IN_PROGRESS"
        task_completed = "COMPLETED"
        task_failed = "FAILED"
        task_pending = "PENDING"
        while self.task_status == task_in_progress:
            logging.info('Task {}: {} {}'.title().format(task_id,custom_task_message,self.task_status))
            time.sleep(10)
            self.dir_task_status(task_id)
        while self.task_status == task_pending:
            logging.info('Task {}: {} {}'.title().format(task_id,custom_task_message,self.task_status))
            time.sleep(5)
            self.dir_task_status(task_id)
        if self.task_status == task_completed:
            logging.info('Task {}: {} {}'.title().format(task_id,custom_task_message,self.task_status))
        elif self.task_status == task_failed:
            logging.info('Task {}: {} {}'.title().format(task_id,custom_task_message,self.task_status))
        else:                 
            logging.info('Task {}: {} {}'.title().format(task_id,custom_task_message,self.task_status))


    def dir_task_status(self,task_id):
        task_status = json.dumps(self.vd.task_status(task_id))
        task_status = json.loads(task_status)
        for key in task_status:
            if 'message' in key:
                logging.error('{}'.title().format(task_status["message"]))
                self.dir_task_status(task_id)  
            if 'error' in key:
                self.dir_task_status(task_id) 
        self.task_status = task_status["versa-tasks.task"]["versa-tasks.task-status"]

    def director_ha_status(self):
        director_ha_status = json.dumps(self.vd.director_ha_status())
        director_ha_status = json.loads(director_ha_status)
        logging.info('Verifying HA Status') 
        if director_ha_status["versanms.VDStatus"]["haConfig"]["startupMode"] == "STANDALONE":
            logging.info('Director is not in HA')
            exit()
        if director_ha_status["versanms.VDStatus"]["haConfig"]["startupMode"] == "HA":
            ha_status_slave_ip = director_ha_status["versanms.VDStatus"]["haDetails"]["peerVnmsHaDetails"][0]["mgmtIpAddress"]
            ha_status_slave_enable = director_ha_status["versanms.VDStatus"]["haDetails"]["peerVnmsHaDetails"][0]["enabled"]
            ha_status_slave_mode = director_ha_status["versanms.VDStatus"]["haDetails"]["peerVnmsHaDetails"][0]["mode"]
            if ha_status_slave_ip == self.slave_dir_mgmt_ip_eth0 and ha_status_slave_enable == True and ha_status_slave_mode == 'SLAVE':
                logging.info('HA Status is good')
                self.user_creation()
            else:
                logging.error('HA Status failed')
                exit()

    def user_creation(self):
        create_user_device_data = {
            "create-user":
            {
                "user":
                {
                    "name": "automation",
                    "firstname": "he",
                    "lastname": "automation",
                    "password": "Versa@123",
                    "email": "automation@versa-networks.com",
                    "idle-time-out": "15",
                    "user-type": "GENERAL",
                    "enable-two-factor": false,
                    "primary-role": "ProviderDataCenterSystemAdmin",
                    "personalized-landing-page": "/appliances",
                    "homedir": ".",
                    "ssh_keydir": "."
                }
            }
        }


        logging.info('creating automation user in director'.title())
        user_creation = json.dumps(self.vd.user_creation(create_user_device_data))
        user_creation = json.loads(user_creation)
        user_creation = user_creation["output"]["result"]["status"]
        if user_creation == "SUCCESS":
            logging.info('Automation user created successfully'.title())
            self.automation_user_pwd = json.dumps(create_user_device_data)
            self.automation_user_pwd = json.loads(self.automation_user_pwd)
            self.automation_username = self.automation_user_pwd["create-user"]["user"]["name"]   
            self.automation_password = self.automation_user_pwd["create-user"]["user"]["password"]  
            self.vd_automation = he_automation_21_2_1(self.master_dir_mgmt_ip_eth0,self.automation_username, self.automation_password) 
        else:
            logging.error('user created failed'.title())
            exit()

    def user_deletion(self):

        logging.info("Automation user deletion started".title())
        logging.info('Deleting automation user in director'.title())
        os.system("echo 'configure\ndelete aaa authentication users user automation\ncommit' | ncs_cli -N")
        logging.info('Automation user deleted successfully'.title()) 

        # user_deletion = {"delete-user":{"name":["automation"]}}
        # logging.info('Deleting automation user in director'.title())
        # user_delete = json.dumps(self.vd.user_deletion(user_deletion))
        # user_delete = json.loads(user_delete)
        # user_delete = user_delete["output"]["result"]["status"]
        # if user_delete == "SUCCESS":
        #     logging.info('Automation user deleted successfully'.title())       
        # else:
        #     logging.error('user deleted failed'.title())             

    def analytics_cluster_configuration(self):
        logging.info('Preparing Analytics cluster configuration'.title())
        analytics_cluster_config = {
                                        "analytics-cluster":
                                        [
                                            {
                                                "name": "analytics_cluster_name",
                                                "log-collector-config":
                                                {
                                                    "port": "1234",
                                                    "ip-address":
                                                    [
                                                        "analytics_1_south_bound_ip_eth1",
                                                        "analytics_2_south_bound_ip_eth1",
                                                        "search_1_south_bound_ip_eth1",
                                                        "search_2_south_bound_ip_eth1"
                                                    ]
                                                },
                                                "connector-config":
                                                {
                                                    "port": "443",
                                                    "web-addresses":
                                                    [
                                                        {
                                                            "name": "Analytics_1",
                                                            "ip-address": "analytics_1_mgnt_ip_eth0"
                                                        },
                                                        {
                                                            "name": "Analytics_2",
                                                            "ip-address": "analytics_2_mgnt_ip_eth0"
                                                        },
                                                        {
                                                            "name": "Search_1",
                                                            "ip-address": "search_1_mgnt_ip_eth0"
                                                        },
                                                        {
                                                            "name": "Search_2",
                                                            "ip-address": "search_2_mgnt_ip_eth0"
                                                        }
                                                    ]
                                                }
                                            }
                                        ]
                                    }

        analytics_cluster_config_update = json.dumps(analytics_cluster_config)
        analytics_cluster_config_update = json.loads(analytics_cluster_config_update)
        analytics_cluster_config_update["analytics-cluster"][0]["name"] = self.van_cluster_name
        analytics_cluster_config_update["analytics-cluster"][0]["log-collector-config"]["ip-address"][0] = self.analytics_1_south_bound_ip_eth1
        analytics_cluster_config_update["analytics-cluster"][0]["log-collector-config"]["ip-address"][1] = self.analytics_2_south_bound_ip_eth1
        analytics_cluster_config_update["analytics-cluster"][0]["log-collector-config"]["ip-address"][2] = self.search_1_south_bound_ip_eth1
        analytics_cluster_config_update["analytics-cluster"][0]["log-collector-config"]["ip-address"][3] = self.search_2_south_bound_ip_eth1
        analytics_cluster_config_update["analytics-cluster"][0]["connector-config"]["web-addresses"][0]["ip-address"] = self.analytics_1_mgnt_ip_eth0
        analytics_cluster_config_update["analytics-cluster"][0]["connector-config"]["web-addresses"][1]["ip-address"] = self.analytics_2_mgnt_ip_eth0
        analytics_cluster_config_update["analytics-cluster"][0]["connector-config"]["web-addresses"][2]["ip-address"] = self.search_1_mgnt_ip_eth0
        analytics_cluster_config_update["analytics-cluster"][0]["connector-config"]["web-addresses"][3]["ip-address"] = self.search_2_mgnt_ip_eth0
        logging.info('Analytics Cluster configuration started'.title())
        analytics_cluster_config_ = json.dumps(self.vd_automation.analytics_cluster_config(self.van_cluster_name,analytics_cluster_config_update))
        analytics_cluster_config_ = json.loads(analytics_cluster_config_)
        if analytics_cluster_config_ == 201:
            logging.info('Analytics Cluster configuration completed'.title())
        else:
            logging.error('Analytics Cluster configuration failed'.title())
            exit()

    def analytics_script(self):
        logging.info('Analytics script preparation initiated'.title())
        os.system("mv /opt/versa/vnms/scripts/van-cluster-config/van_cluster_install/clustersetup.conf /opt/versa/vnms/scripts/van-cluster-config/van_cluster_install/clustersetup.conf_bak")
        analytics_cluster_conf = open('analytics_cluster_conf')
        analytics_cluster_conf = analytics_cluster_conf.read()
        checkWords = ("van_cluster_name","master_dir_mgmt_ip","master_dir_south_bound_ip","slave_dir_mgmt_ip","slave_dir_south_bound_ip","analytics_1_hostname","analytics_1_mgnt_ip","analytics_1_south_bound_ip","analytics_2_hostname","analytics_2_mgnt_ip","analytics_2_south_bound_ip","search_1_hostname","search_1_mgnt_ip","search_1_south_bound_ip","search_2_hostname","search_2_mgnt_ip","search_2_south_bound_ip")
        repWords = (self.van_cluster_name,self.master_dir_mgmt_ip_eth0,self.master_dir_south_bound_ip_eth1,self.slave_dir_mgmt_ip_eth0,self.slave_dir_south_bound_ip_eth1,self.analytics_1_hostname,self.analytics_1_mgnt_ip_eth0,self.analytics_1_south_bound_ip_eth1,self.analytics_2_hostname,self.analytics_2_mgnt_ip_eth0,self.analytics_2_south_bound_ip_eth1,self.search_1_hostname,self.search_1_mgnt_ip_eth0,self.search_1_south_bound_ip_eth1,self.search_2_hostname,self.search_2_mgnt_ip_eth0,self.search_2_south_bound_ip_eth1)
        analytics_cluster_conf_update = open("clustersetup.conf", "wt")        
        for line in analytics_cluster_conf.splitlines():
            # read replace the string and write to output file
            for check, rep in zip(checkWords, repWords):
                line = line.replace(check, rep)
            analytics_cluster_conf_update.write(line)
            analytics_cluster_conf_update.write('\n')
        analytics_cluster_conf_update.close()
        os.system("cp clustersetup.conf /opt/versa/vnms/scripts/van-cluster-config/van_cluster_install/clustersetup.conf")
        os.system("chown versa:versa /opt/versa/vnms/scripts/van-cluster-config/van_cluster_install/clustersetup.conf")

        analytics_cluster_script_sh = """
        cd /opt/versa/vnms/scripts/van-cluster-config/van_cluster_install/
        ./van_cluster_installer.py --force
        sleep 60
        ./van_cluster_installer.py --post-setup --gen-vd-cert
        """
        #Generate ha_service_check_script.sh 
        time.sleep(3)
        analytics_cluster_script_sh_file = open("analytics_cluster_script.sh", "wt")
        analytics_cluster_script_sh_file.write(analytics_cluster_script_sh)
        analytics_cluster_script_sh_file.close()
        os.system("chmod +x analytics_cluster_script.sh")
        os.system("./analytics_cluster_script.sh")
        logging.info('Analytics script preparation Completed'.title())
        os.remove("clustersetup.conf")

    def device_creation(self):
        # logging.info('DC service vnf resource creation Started'.title())
        device_creation_data = {
                                    "localOrg":
                                    {
                                        "name": "service_vnf_name",
                                        "uuid": "device_uuid",
                                        "localOrgNetworks":
                                        [
                                            {
                                                "name": "Towards_which_he_pri_sec",
                                                "ipAddressAllocationMode": "manual",
                                                "ipAddressAllocationModev6": "manual"
                                            }
                                        ],
                                        "localInstances":
                                        [
                                            {
                                                "name": "service_vnf_name",
                                                "ipAddress": "service_vnf_eth0_mgnt_ip"
                                            }
                                        ]
                                    }
                                }
        #DC-Service_data_update
        self.dcuuid = uuid.uuid4()
        dc_service_vnf_creation = json.dumps(device_creation_data)
        dc_service_vnf_creation = json.loads(dc_service_vnf_creation)
        dc_service_vnf_creation["localOrg"]["name"] = self.dc_service_vnf_name
        dc_service_vnf_creation["localOrg"]["uuid"] = str(self.dcuuid)
        dc_service_vnf_creation["localOrg"]["localOrgNetworks"][0]["name"] = "To_Master_Director"
        dc_service_vnf_creation["localOrg"]["localInstances"][0]["name"] = self.dc_service_vnf_name
        dc_service_vnf_creation["localOrg"]["localInstances"][0]["ipAddress"] = self.dc_service_vnf_mgnt_ip_eth0
        logging.info('DC Service VNF creation started')
        dc_service_vnf_device_creation = json.dumps(self.vd_automation.device_creation(dc_service_vnf_creation))
        dc_service_vnf_device_creation = json.loads(dc_service_vnf_device_creation)
        if dc_service_vnf_device_creation == 200:
            logging.info('DC Service VNF resource Created successfully')
        else:
            logging.error('DC Service VNF resource Creation failed')
            exit()
        #DR-Service_data_update
        self.druuid = uuid.uuid4()
        dr_service_vnf_creation = json.dumps(device_creation_data)
        dr_service_vnf_creation = json.loads(dr_service_vnf_creation)
        dr_service_vnf_creation["localOrg"]["name"] = self.dr_service_vnf_name
        dr_service_vnf_creation["localOrg"]["uuid"] = str(self.druuid)
        dr_service_vnf_creation["localOrg"]["localOrgNetworks"][0]["name"] = "To_Master_Director"
        dr_service_vnf_creation["localOrg"]["localInstances"][0]["name"] = self.dr_service_vnf_name
        dr_service_vnf_creation["localOrg"]["localInstances"][0]["ipAddress"] = self.dr_service_vnf_mgnt_ip_eth0
        logging.info('DR service VNF creation started')
        dr_service_vnf_device_creation = json.dumps(self.vd_automation.device_creation(dr_service_vnf_creation))
        dr_service_vnf_device_creation = json.loads(dr_service_vnf_device_creation)
        if dr_service_vnf_device_creation == 200:
            logging.info('DR Service VNF Created successfully')
        else:
            logging.error('DR Service VNF Creation failed')
            exit()

    def org_creation(self):
        org_creation_data = {
                                    "id": 10,
                                    "name": "parent_org_name",
                                    "authType": "psk",
                                    "dynamicTenantConfig":
                                    {
                                        "inactivityInterval": 48
                                    },
                                    "subscriptionPlan": "Default-All-Services-Plan",
                                    "vrfsGroups":
                                    [
                                        {
                                            "name": "{parent_org_name}-LAN-VR",
                                            "vrfId": "1",
                                            "enable_vpn": "true"
                                        }
                                    ],
                                    "analyticsClusters":
                                    [
                                        "VAN-Cluster"
                                    ],
                                    "sharedControlPlane": false,
                                    "cpeDeploymentType": "SDWAN",
                                    "cmsOrgs":
                                    [
                                        {
                                            "name": "dc_service_vnf_name",
                                            "uuid": "dcuuid"
                                        },
                                        {
                                            "name": "dc_service_vnf_name",
                                            "uuid": "druuid"
                                        }
                                    ]
                                }
        #ORG_data_update
        parent_org_lan_vr = self.parent_org_name+"-LAN-VR"
        org_creation = json.dumps(org_creation_data)
        org_creation = json.loads(org_creation)
        org_creation["name"] = self.parent_org_name
        org_creation["vrfsGroups"][0]["name"] = parent_org_lan_vr
        org_creation["cmsOrgs"][0]["name"] = self.dc_service_vnf_name
        org_creation["cmsOrgs"][0]["uuid"] = str(self.dcuuid)
        org_creation["cmsOrgs"][1]["name"] = self.dr_service_vnf_name
        org_creation["cmsOrgs"][1]["uuid"] = str(self.druuid)
        logging.info('ORG Creation Started')
        org_creation = json.dumps(self.vd_automation.org_creation(org_creation))
        org_creation = json.loads(org_creation)
        if org_creation == 200:
            logging.info('ORG Created Successfully')
        else:
            logging.error('ORG Creation Failed')
            exit()

    def org_fetch(self):
        logging.info('Fetching ORG Details from Director')
        org_fetch = json.dumps(self.vd_automation.org_fetch(self.parent_org_name))
        org_fetch = json.loads(org_fetch)
        for org in org_fetch["organizations"]:
            if org["name"] == self.parent_org_name:
                self.parent_org_uuid = org["uuid"]

    def device_addition(self):
        logging.info('DC service VNF resource addition Started')
        device_addition_data = {
                                    "add-devices":
                                    {
                                        "devices":
                                        {
                                            "device":
                                            {
                                                "mgmt-ip": "service_vnf_eth0_mgnt_ip",
                                                "name": "service_vnf_name",
                                                "org": "orguuid",
                                                "cmsorg": "device_uuid",
                                                "type": "service-vnf",
                                                "discover": "true",
                                                "snglist":
                                                {
                                                    "sng":
                                                    [
                                                        {
                                                            "name": "Default_All_Services",
                                                            "isPartOfVCSN": true
                                                        }
                                                    ]
                                                },
                                                "subscription":
                                                {
                                                    "solution-tier": "NetBase",
                                                    "bandwidth": 50,
                                                    "license-period": 1,
                                                    "custom-param":
                                                    []
                                                }
                                            }
                                        }
                                    }
                                }
        #DC-Service_data_update
        dc_service_vnf_addition = json.dumps(device_addition_data)
        dc_service_vnf_addition = json.loads(dc_service_vnf_addition)
        dc_service_vnf_addition["add-devices"]["devices"]["device"]["mgmt-ip"] = self.dc_service_vnf_mgnt_ip_eth0
        dc_service_vnf_addition["add-devices"]["devices"]["device"]["name"] = self.dc_service_vnf_name
        dc_service_vnf_addition["add-devices"]["devices"]["device"]["org"] = self.parent_org_uuid
        dc_service_vnf_addition["add-devices"]["devices"]["device"]["cmsorg"] = str(self.dcuuid)
        logging.info('DC service VNF addition started')
        dc_service_vnf_device_addition = json.dumps(self.vd_automation.device_addition(dc_service_vnf_addition))
        dc_service_vnf_device_addition = json.loads(dc_service_vnf_device_addition)
        dc_service_vnf_device_addition_task_id = dc_service_vnf_device_addition["output"]["result"]["task"]["task-id"]
        self.dir_task_status_monitor("DC Service VNF Deploy Status",dc_service_vnf_device_addition_task_id)  
        #dr-Service_data_update
        dr_service_vnf_addition = json.dumps(device_addition_data)
        dr_service_vnf_addition = json.loads(dr_service_vnf_addition)
        dr_service_vnf_addition["add-devices"]["devices"]["device"]["mgmt-ip"] = self.dr_service_vnf_mgnt_ip_eth0
        dr_service_vnf_addition["add-devices"]["devices"]["device"]["name"] = self.dr_service_vnf_name
        dr_service_vnf_addition["add-devices"]["devices"]["device"]["org"] = self.parent_org_uuid
        dr_service_vnf_addition["add-devices"]["devices"]["device"]["cmsorg"] = str(self.druuid)
        logging.info('DR service VNF addition started')
        dr_service_vnf_device_addition = json.dumps(self.vd_automation.device_addition(dr_service_vnf_addition))
        dr_service_vnf_device_addition = json.loads(dr_service_vnf_device_addition)
        dr_service_vnf_device_addition_task_id = dr_service_vnf_device_addition["output"]["result"]["task"]["task-id"]
        self.dir_task_status_monitor("DR Service VNF Deploy Status",dr_service_vnf_device_addition_task_id)          

    def route_addition(self):
        logging.info('Adding static routes for overlay subnets'.title())
        static_route_data = {
                                "route":
                                {
                                    "description": "route for overlay_prefix",
                                    "destination-prefix": "overlay_prefixes",
                                    "next-hop-address": "service_vnf_south_bound_ip",
                                    "outgoing-interface": "eth1"
                                }
                            }

        #DC-static_route_data_update
        dc_overlay_route_data_update = json.dumps(static_route_data)
        dc_overlay_route_data_update = json.loads(dc_overlay_route_data_update)
        dc_overlay_route_data_update["route"]["destination-prefix"] = str(self.overlay_subnet)
        dc_overlay_route_data_update["route"]["next-hop-address"] = self.dc_service_vnf_towards_director_eth1
        logging.info('Adding overlay subnet route in DC')
        dc_overlay_route_addition = json.dumps(self.vd_automation.static_route_addition(dc_overlay_route_data_update))
        dc_overlay_route_addition = json.loads(dc_overlay_route_addition)
        if dc_overlay_route_addition == 201:
            logging.info('Overlay subnet route Added successfully in DC')
            #dr-static_route_data_update
            dr_overlay_route_data_update = json.dumps(static_route_data)
            dr_overlay_route_data_update = json.loads(dr_overlay_route_data_update)
            dr_overlay_route_data_update["route"]["destination-prefix"] = str(self.overlay_subnet)
            dr_overlay_route_data_update["route"]["next-hop-address"] = self.dr_service_vnf_towards_director_eth1

            logging.info('Adding overlay subnet route in DR')
            dr_overlay_route_addition = json.dumps(self.vd_automation.static_route_addition(dr_overlay_route_data_update))
            dr_overlay_route_addition = json.loads(dr_overlay_route_addition)
            if dr_overlay_route_addition == 201:
                logging.info('Overlay subnet route Added successfully in DR')
            else:
                logging.error('Overlay subnet route Addition failed in DR')
        else:
            logging.error('Overlay subnet route Addition failed in DC')
        self.overlay_address_modification()

    def overlay_address_modification(self):
        logging.info('Checking overlay subnet'.title())
        overlay_address_fetch = json.dumps(self.vd_automation.overlay_address_fetch())
        overlay_address_fetch = json.loads(overlay_address_fetch)
        if not overlay_address_fetch["prefixes"]:
            self.overlay_address_addition()
        else:
            logging.info('Default overlay subnet deletion'.title())
            default_overlay_route_deletion = json.dumps(self.vd_automation.overlay_address_deletion())
            default_overlay_route_deletion = json.loads(default_overlay_route_deletion)
            if default_overlay_route_deletion == 201:
                logging.info('Default overlay subnet deleted successfully'.title())
            else:
                logging.error('Default overlay subnet deletion failed'.title())  
                exit()     

    def overlay_address_addition(self):
        overlay_subnet_data = {
                                    "prefix": "${overlay_prefixes}",
                                    "status":
                                    {
                                        "label": "Active"
                                    }
                                }    
        logging.info('overlay subnets addition started'.title())
        overlay_subnet_data_update = json.dumps(overlay_subnet_data)
        overlay_subnet_data_update = json.loads(overlay_subnet_data_update)
        overlay_subnet_data_update["prefix"] = str(self.overlay_subnet)
        overlay_subnet_data_update = json.dumps(self.vd_automation.overlay_address_addition(overlay_subnet_data_update))
        overlay_subnet_data_update = json.loads(overlay_subnet_data_update)
        if overlay_subnet_data_update == 200:
            logging.info('overlay subnet Added successfully'.title())
            self.wan_network_data_update()
        else:
            logging.error('overlay subnet Addition failed'.title())
            exit()

    def wan_network_data_update(self):
        self.org_fetch()
        logging.info('WAN Network creation started')
        wan_network = {
                            "name": "network_name",
                            "transport-domains":
                            [
                                "domain_name"
                            ]
                        }
        mpls_network_data = json.dumps(wan_network)
        mpls_network_data = json.loads(mpls_network_data)
        mpls_network_data["name"] = "MPLS"
        mpls_network_data["transport-domains"][0] = "MPLS"
        self.wan_network_creation("MPLS",mpls_network_data)
        internet_network_data = json.dumps(wan_network)
        internet_network_data = json.loads(internet_network_data)
        internet_network_data["name"] = "Internet"
        internet_network_data["transport-domains"][0] = "Internet"
        self.wan_network_creation("Internet",internet_network_data)

    def wan_network_creation(self,network_name,network_data):
        logging.info('{} Network creation started'.title().format(network_name))
        network_creation = json.dumps(self.vd_automation.wan_network(self.parent_org_uuid,network_data))
        network_creation = json.loads(network_creation)
        if network_creation == 200:
            logging.info('{} Network created successfully'.title().format(network_name))
            time.sleep(20)
            self.controller_save_deploy_data_update()
        else:
            logging.error('{} Network creation failed'.title().format(network_name))
            exit()

    def controller_deploy(self):
            controller_1_deploy = json.dumps(self.vd_automation.controller_deploy(self.controller_2_hostname))
            controller_1_deploy = json.loads(controller_1_deploy)
            print(controller_1_deploy)

    def controller_save_deploy_data_update(self):
        logging.info('controller creation started'.title())
        controller_data = open('controller_data.json')
        controller_data = controller_data.read()
        #controller 1 data update
        dc_controll_subnet_prefixlen = ipaddress.IPv4Network(self.dc_controll_subnet).prefixlen
        dc_internet_subnet_prefixlen = ipaddress.IPv4Network(self.dc_internet_subnet).prefixlen
        dc_mpls_subnet_prefixlen = ipaddress.IPv4Network(self.dc_mpls_subnet).prefixlen
        controller_1_controll_network_ip_eth1_with_prefixlen = self.controller_1_controll_network_ip_eth1+"/"+str(dc_controll_subnet_prefixlen)
        controller_1_wan_1_ip_eth2_with_prefixlen = self.controller_1_wan_1_ip_eth2+"/"+str(dc_internet_subnet_prefixlen)
        controller_1_wan_2_ip_eth3_with_prefixlen = self.controller_1_wan_2_ip_eth3+"/"+str(dc_mpls_subnet_prefixlen)
        checkWords = ("controller_hostname","parent_org_name","site_id","van_cluster_name","controller_country","service_vnf_south_bound_ip","controller_mgnt_ip","controller_controll_network_ip_eth1","controller_wan_1_ip_eth2","controller_wan_1_gateway","controller_internet_public_ip","controller_wan_2_ip_eth3","controller_wan_2_gateway")
        repWords = (self.controller_1_hostname,self.parent_org_name,str(1),self.van_cluster_name,self.controller_1_country,self.dc_service_vnf_towards_controller_eth3,self.controller_1_mgnt_ip_eth0,controller_1_controll_network_ip_eth1_with_prefixlen,controller_1_wan_1_ip_eth2_with_prefixlen,self.controller_1_wan_1_ip_eth2_gateway,self.controller_1_internet_public_ip,controller_1_wan_2_ip_eth3_with_prefixlen,self.controller_1_wan_2_ip_eth3_gateway)
        #for each line in the input file
        controller_1_data_update_json = open("controller_1_data_update_json", "wt")
        for line in controller_data.splitlines():
            # read replace the string and write to output file
            for check, rep in zip(checkWords, repWords):
                line = line.replace(check, rep)
            controller_1_data_update_json.write(line)
        controller_1_data_update_json.close()
        controller_1_data_update_json = open("controller_1_data_update_json","rt")
        controller_1_data_update_json = json.loads(controller_1_data_update_json.read())
        if len(self.controller_1_internet_public_ip) == 0:
            self.controller_1_save_deploy(controller_1_data_update_json)
        else:
            controller_1_data_update_json["versanms.sdwan-controller-workflow"]["baremetalController"]["wanInterfaces"][0]["unitInfoList"][0]["publicIPAddress"] = self.controller_1_internet_public_ip
            self.controller_1_save_deploy(controller_1_data_update_json)
        #controller 2 data update
        dr_controll_subnet_prefixlen = ipaddress.IPv4Network(self.dr_controll_subnet).prefixlen
        dr_internet_subnet_prefixlen = ipaddress.IPv4Network(self.dr_internet_subnet).prefixlen
        dr_mpls_subnet_prefixlen = ipaddress.IPv4Network(self.dr_mpls_subnet).prefixlen
        controller_2_controll_network_ip_eth1_with_prefixlen = self.controller_2_controll_network_ip_eth1+"/"+str(dr_controll_subnet_prefixlen)
        controller_2_wan_1_ip_eth2_with_prefixlen = self.controller_2_wan_1_ip_eth2+"/"+str(dr_internet_subnet_prefixlen)
        controller_2_wan_2_ip_eth3_with_prefixlen = self.controller_2_wan_2_ip_eth3+"/"+str(dr_mpls_subnet_prefixlen)

        checkWords = ("controller_hostname","parent_org_name","site_id","van_cluster_name","controller_country","service_vnf_south_bound_ip","controller_mgnt_ip","controller_controll_network_ip_eth1","controller_wan_1_ip_eth2","controller_wan_1_gateway","controller_internet_public_ip","controller_wan_2_ip_eth3","controller_wan_2_gateway")
        repWords = (self.controller_2_hostname,self.parent_org_name,str(2),self.van_cluster_name,self.controller_2_country,self.dr_service_vnf_towards_controller_eth3,self.controller_2_mgnt_ip_eth0,controller_2_controll_network_ip_eth1_with_prefixlen,controller_2_wan_1_ip_eth2_with_prefixlen,self.controller_2_wan_1_ip_eth2_gateway,self.controller_2_internet_public_ip,controller_2_wan_2_ip_eth3_with_prefixlen,self.controller_2_wan_2_ip_eth3_gateway)
        #for each line in the input file
        controller_2_data_update_json = open("controller_2_data_update_json", "wt")        
        for line in controller_data.splitlines():
            for check, rep in zip(checkWords, repWords):
                line = line.replace(check, rep)
            controller_2_data_update_json.write(line)
        controller_2_data_update_json.close()
        controller_2_data_update_json = open("controller_2_data_update_json","rt")
        controller_2_data_update_json = json.loads(controller_2_data_update_json.read())
        if len(self.controller_2_internet_public_ip) == 0:
            self.controller_2_save_deploy(controller_2_data_update_json)
        else:
            controller_2_data_update_json["versanms.sdwan-controller-workflow"]["baremetalController"]["wanInterfaces"][0]["unitInfoList"][0]["publicIPAddress"] = self.controller_1_internet_public_ip
            self.controller_2_save_deploy(controller_2_data_update_json)        
        exit()   

    def controller_1_save_deploy(self,controller_1_data_update_json):        
        logging.info('controller 1 save initiated'.title())
        controller_1_save = json.dumps(self.vd_automation.controller_save(controller_1_data_update_json))
        if int(controller_1_save) == 200:
            logging.info('controller 1 saved successfully'.title())
            logging.info('controller 1 deploy initiated'.title())
            controller_1_deploy = json.dumps(self.vd_automation.controller_deploy(self.controller_1_hostname))
            controller_1_deploy = json.loads(controller_1_deploy)
            controller_1_deploy_task_id = controller_1_deploy["TaskResponse"]["task-id"]
            self.dir_task_status_monitor("Controller 1 Deploy Status",controller_1_deploy_task_id)  
            os.remove("controller_1_data_update_json") 
        else:
            logging.error('Controller 1 {} status code'.format(controller_1_save))
            
    def controller_2_save_deploy(self,controller_2_data_update_json):  
            logging.info('controller 2 save initiated'.title())
            controller_2_save = json.dumps(self.vd_automation.controller_save(controller_2_data_update_json))
            if int(controller_2_save) == 200:
                logging.info('controller 2 saved successfully'.title())
                logging.info('controller 2 deploy initiated'.title())
                controller_2_deploy = json.dumps(self.vd_automation.controller_deploy(self.controller_2_hostname))
                controller_2_deploy = json.loads(controller_2_deploy)
                controller_2_deploy_task_id = controller_2_deploy["TaskResponse"]["task-id"]
                self.dir_task_status_monitor("Controller 2 Deploy Status",controller_2_deploy_task_id)
                os.remove("controller_2_data_update_json")
                self.analytics_script()
            else:
                logging.error('Controller 2 {} status code'.format(controller_2_save))
                exit()

def main():
    automation = full_automaton()
    try:
        if automation.with_flex:
            automation.service_vnf_rechability_check()
            automation.service_vnf_1()
            automation.rechability_check()
            automation.director_service_status()  
            automation.analytics_cluster_configuration()
            automation.device_creation()
            automation.org_creation()
            automation.org_fetch()
            automation.device_addition()       
            automation.route_addition()
            automation.user_deletion()
        if automation.with_out_flex:
            automation.rechability_check()
            automation.director_service_status()
            automation.analytics_cluster_configuration()
            automation.org_creation()
            automation.org_fetch()
            automation.route_addition()   
            automation.user_deletion()    
    except AttributeError:
        exit()       
if __name__ == "__main__":
    main()